import { tap, expect } from '@pushrocks/tapbundle';
import * as lik from '../ts/index.js';

let testAsyncExecutionStack: lik.AsyncExecutionStack;

tap.test('should create a valid instance of AsyncExectionStack', async () => {
  testAsyncExecutionStack = new lik.AsyncExecutionStack();
  expect(testAsyncExecutionStack).toBeInstanceOf(lik.AsyncExecutionStack);
});

tap.test('should run in parallel', async (toolsArg) => {
  await testAsyncExecutionStack.getExclusiveExecutionSlot(async () => {
    await toolsArg.delayFor(2000);
    console.log('should run first');
  }, 2500);
  testAsyncExecutionStack.getNonExclusiveExecutionSlot(async () => {
    await toolsArg.delayFor(2000);
    console.log('should run third');
  }, 2500);
  testAsyncExecutionStack.getNonExclusiveExecutionSlot(async () => {
    await toolsArg.delayFor(1000);
    console.log('should run second');
  }, 2500);
  await testAsyncExecutionStack.getExclusiveExecutionSlot(async () => {
    console.log('should run fourth');
  }, 0);
});

tap.start();
