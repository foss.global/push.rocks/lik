// import test framework
import { expect, tap } from '@pushrocks/tapbundle';

// import the module
import * as lik from '../ts/index.js';

let object1 = {};
let object2 = {};
let myLoopTracker: lik.LoopTracker<any>;

// tests
tap.test('should create a valid looptracker instance', async () => {
  myLoopTracker = new lik.LoopTracker();
  expect(myLoopTracker).toBeInstanceOf(lik.LoopTracker);
});

tap.test('should add objects once and return true', async () => {
  expect(myLoopTracker.checkAndTrack(object1)).toBeTrue();
  expect(myLoopTracker.checkAndTrack(object1)).toBeFalse();
  expect(myLoopTracker.checkAndTrack(object2)).toBeTrue();
  expect(myLoopTracker.checkAndTrack(object2)).toBeFalse();
});

tap.start();
