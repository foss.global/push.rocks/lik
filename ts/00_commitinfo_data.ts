/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/lik',
  version: '6.0.3',
  description: 'light little helpers for node'
}
